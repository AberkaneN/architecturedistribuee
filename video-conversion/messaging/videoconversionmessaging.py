import time

import pika
from threading import Thread
import logging
import json
import queue
from google.api_core.exceptions import DeadlineExceeded
from google.cloud import pubsub_v1

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
logging.getLogger("pika").setLevel(logging.INFO)


# rabbitmqadmin -H localhost -u ezip -p pize -V ezip purge queue name=video-conversion-queue
# rabbitmqadmin -H localhost -u ezip -p pize -V ezip get queue=video-conversion-queue

class VideoConversionMessaging(Thread):
    def __init__(self, _config_, converting_service):
        Thread.__init__(self)
        self.project_id = _config_.get_pubsub_project()
        self.subscription_name = _config_.get_pubsub_subscription()
        self.converting_service = converting_service
        self.subscriber = pubsub_v1.SubscriberClient()
        self.subscription_path = self.subscriber.subscription_path(self.project_id, self.subscription_name)
        self.flow_control = pubsub_v1.types.FlowControl(max_messages=1)
        self.start()

    def run(self):
        logging.info("Starts consuming message")
        while True:
            try:
                response = self.subscriber.pull(self.subscription_path, max_messages=1)
                for msg in response.received_messages:
                    self.subscriber.acknowledge(self.subscription_path, [msg.ack_id])
                    the_message = msg.message.data
                    self._on_message_(the_message)
            except DeadlineExceeded:
                print("Aucun message")
            time.sleep(2)

    def on_message(self, channel, method_frame, header_frame, body):
        logging.info(body)
        # logging.info('id = %s, URI = %s', body["id"], body['originPath'])
        # logging.info('URI = %s', body['originPath'])
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    def _on_message_(self, body):
        logging.info(body)
        convert_request = json.loads(body.decode())
        # logging.info('id = %s, URI = %s', convert_request["id"], convert_request['originPath'])
        # logging.info('URI = %s', body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    def stop_consuming(self):
        logging.info("Stops consuming on message bus")
        self.channel.stop_consuming()
        self.consuming = "_IDLE_"

    def start_consuming(self):
        logging.info("Starts consuming on message bus")
        self.channel.start_consuming()
        self.rendez_vous.put("_CONSUMING_")
        self.consuming = "_CONSUMING_"
        self.start()

    def is_consuming(self):
        return self.consuming
